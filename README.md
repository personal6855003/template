# template

For overview, the maven-multi-module-change-trigger-template is intended to be used in the parent project's .gitlab-ci.yml.

Each child module is expected to have a .gitlab-ci.yml so that a child pipeline can be triggered by the jobs that will be 
added by the aforementioned template.

The main idea is that the maven-multi-module-change-trigger-template will check if there are any changes in the child module, then trigger the child pipeline.
At the same time, it passes a trigger variable TRIGGER containing either "yes" or "no" to the child pipeline.

The child pipeline may choose to use maven-multi-module-child-pipeline-template to have an already configured build and deploy job that works with the
trigger variable TRIGGER. Alternatively, it can be customized to make use of the trigger variable TRIGGER.

## Prerequisites

You will need to create 2 file type CICD variables in your project:

1. `SSH_PRIVATE_KEY`
Create a local key pair and add the private key to this variable (file must end with newline). The public key should be added to the project as a deploy key.
2. `SSH_KNOWN_HOSTS`
Run the command `ssh-keyscan gitlab.com` and add the output to this variable (file must end with newline).

You will also need to have this global variable configured in your .gitlab-ci.yml

1. `MAVEN_CLI_OPTS`
Your maven command options

## Overview

The following table provides brief details of the templates available in this repository.

| file                                                    | description                                                                                                                                                                                                                                                                                                                                                                                       | inputs                                                                          | artifacts                                                                                                     | remarks                                                                                                                                                                    |
|---------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| maven-multi-module-build-template.yml                   | 1. Install the parent pom<br/>2. Build and test child module                                                                                                                                                                                                                                                                                                                                      | 1. module-folder<br/>2. stage<br/>3. expires-in                                 | 1. surefire reports<br/>2. jacoco coverage reports<br/>3. build.env (variable indicating status of job added) | Can be toggled on/off using trigger variable TRIGGER.<br/>Otherwise, toggled on by default                                                                                 |
| maven-multi-module-package-registry-deploy-template.yml | 1. Deploys child module package to package registry                                                                                                                                                                                                                                                                                                                                               | 1. module-folder<br/>2. stage                                                   | -                                                                                                             | Can be toggled on/off using trigger variable TRIGGER.<br/>Also requires that pipeline is not a merge request pipeline and that the commit branch is the default branch     |
| maven-multi-module-child-pipeline-template.yml          | 1. Includes maven-multi-module-build-template.yml<br/>2. Includes maven-multi-module-package-registry-deploy-template.yml<br/>3. Provides dummy job that runs when TRIGGER="no"                                                                                                                                                                                                                   | 1. module-folder<br/>2. build-stage<br/>3. deploy-stage<br/>4. build-expires-in | 1. (same as maven-multi-module-build-template)<br/>                                                           | Provides a dummy job that runs when the trigger variable TRIGGER="no" is set. This ensures child pipeline is not empty when TRIGGER="no"                                   |
| maven-multi-module-change-trigger-template.yml          | 1. Checks if there are any changes in child module between commit before pipeline and the commit that the pipeline is being run for default branch pipelines, and between the non-default branch and the default branch for non-default branch pipelines, and between the source and target branch for merge request pipelines<br/>2. Runs trigger jobs to trigger child pipeline of child module | 1. module-folder<br/>2. change-detection-stage<br/>3. trigger-stage             | -                                                                                                             | This template is intended to be used in the parent project's .gitlab-ci.yml together with the maven-multi-module-child-template used in each child module's .gitlab-ci.yml |
